class Animal:
    def __init__(self, name) -> None:
        self.__name = name

    @property
    def name(self):
        return self.__name

    def sleep(self):
        print(f"{self.__name} is sleeping 3h")

class Cat(Animal):
    def __init__(self, name, color) -> None:
        super().__init__(name)
        self.__color = color

    def pur(self):
        print(f"{self.name} pur")

    def sleep(self):
        super().sleep()
        print(f"{self.name} is sleeping 9 h")

c = Cat("Lucky", "Black")
c.sleep()
c.pur()
