class Renderable:
    def __init__(self) -> None:
        self.render_object = "ASDF"
        print(self.render_object)

class Renderable2D(Renderable):
    def __init__(self) -> None:
        super().__init__()

class Renderable3D():
    def __init__(self) -> None:
        # super().__init__()
        self.render_object = "test"

class Renderable2dAnd3D(Renderable2D, Renderable3D):
    def __init__(self) -> None:
        Renderable2D.__init__(self)
        Renderable3D.__init__(self)
        print(self.render_object)
    

test = Renderable2dAnd3D()