class Car:
    def __init__(self, brand, model, max_speed, min_speed):
        self.__brand = ""
        self.brand = brand
        self.__model = model
        self.__max_speed = max_speed
        self.__min_speed = min_speed
        self.current_speed = 0
        self.base_accel = 20
        self.current_accel = self.base_accel
        self.current_gear = 1

    @property
    def current_speed(self):
        return self.__current_speed
    
    @current_speed.setter
    def current_speed(self, value):
        if(value <= self.max_speed and value >= self.min_speed):
            self.__current_speed = value
        else:
            if(value > self.max_speed):
                self.__current_speed = self.max_speed
            else:
                self.__current_speed = self.min_speed

    @property
    def brand(self):
        return self.__brand
    
    @brand.setter
    def brand(self, value):
        if(value == "Mercedes" or value == "Audi"):
            self.__brand = value

    @property
    def model(self):
        return self.__model
    
    @model.setter
    def model(self, value):
        self.__model = value

    @property
    def max_speed(self):
        return self.__max_speed
    
    @max_speed.setter
    def max_speed(self, value):
        self.__max_speed = value


    @property
    def min_speed(self):
        return self.__min_speed
    
    @min_speed.setter
    def min_speed(self, value):
        self.__min_speed = value


        # if(value <= self.max_speed and value >= self.min_speed):

    def accelerate(self):
        self.__current_speed += self.current_accel

        if self.__current_speed > self.max_speed:
            self.__current_speed = self.max_speed

    def breaking(self):
        self.__current_speed -= self.current_accel
        if self.__current_speed < self.min_speed:
            self.__current_speed = self.min_speed

    def change_gear_up(self):
        self.current_gear += 1
        self.current_accel = self.base_accel * self.current_gear

    def change_gear_down(self):
        self.current_gear -= 1
        self.current_accel = self.base_accel * self.current_gear

    def __repr__(self):
        return f"{self.brand} {self.model} {self.current_speed}"
    
    # def __str__(self):
    #     return f"{self.brand} {self.model} {self.max_speed} {self.min_speed}"
    
    def __eq__(self, __value: object) -> bool:
        if(isinstance(__value, Car)):
            return __value.model == self.model and __value.brand == self.brand
        else:
            return False
        
v1 = Car("Audi", "asdf", 100, -30)
v1.current_speed = 230
v2 = Car("Mercedes", "asdf", 200, -30)
v2.current_speed = 100
print(v1.brand, v1.model)
print(v1)
print("Hello -> " + str(v1))
print(f"Hello -> {v1}")
print(v1 == v2)