class Professor:
    def __init__(self, last_name, first_name, age: int = 42):
        self.last_name = last_name
        self.first_name = first_name
        self.age = age
        self.alive = True

p = Professor("Picous", "Balthazar", 30)
p2 = Professor("Picous", "Fifi", "asdf")
p3 = Professor("Picous", "Zaza")
print(p.first_name, p.last_name, p.age)
print(p2.first_name, p2.last_name, p2.age)
print(p3.first_name, p3.last_name, p3.age)