# a et b vont être des entiers
stop = 0

while stop == 0:
    a = int(input("Entrer le premier nombre : "))

    operator = ""

    # l'opérateur sera une string égale à "+" "-" "/" "*"
    # initialisation de l'opérateur
    # while operator not in ["+", "-", "*", "/"]:
    while operator != "+" and operator != "-" and operator != "*" and operator != "/":
        operator = input("Entrer l'opérateur : ")

    b = int(input("Entrer le second nombre : "))

    if operator == "+":
        print(a + b)
    elif operator == "-":
        print(a - b)
    elif operator == "*":
        print(a * b)
    elif operator == "/":
        if b != 0:
            print(a / b)
        else:
            print("Error division par zéro")

    stop = int(input("Voulez vous continuer ? 0/1"))