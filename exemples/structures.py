my_dict = {
    0: "donald",
    1: "riri",
    2: "fifi",
    3: "loulou"
}

my_dict2 = {
    "firstname": "Philippe",
    "lastname": "Stons",
    "company": "Bstorm"
}

my_dict3 = {
    "firstname": "FLavian",
    "lastname": "Ovyn",
    "company": "Bstorm"
}

my_dict4 = {
    "stons_philippe": my_dict2,
    "ovyn_flavian": my_dict3,
}

print(my_dict2)
my_dict2["Matricule"] = "ps2023"
print(my_dict2)

for key, val in my_dict2.items():
    if(key.lower() == "MAtricule".lower()):
        print("MATRICULE")
    print(key.lower(), val, sep=" -> ")