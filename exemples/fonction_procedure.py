def my_procedure(param):
    print(param * 2)

def my_function(param):
    param = param + param * 2
    print(param)
    return param

var1 = 5
my_procedure(var1)
print(var1)
my_var = my_function(var1)
print(var1)
print(my_var)