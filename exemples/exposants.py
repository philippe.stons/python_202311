tab = []

i = 1
while i <= 10:
    tab.append(2 ** i)
    i += 1


i = 0
while i < 10:
    print(tab[i])
    i += 1

tab_len = len(tab)
for i in range((tab_len // 2) + 1):
    # tab[i], tab[tab_len - 1 - i] = tab[tab_len - 1 - i], tab[i]
    tmp = tab[i]
    tab[i] = tab[tab_len - 1 - i]
    tab[tab_len - 1 - i] = tmp

print(tab)

tab2 = []

for i in range(len(tab) - 1, - 1, -1):
    tab2.append(tab[i])

tab.reverse()
print(tab)
