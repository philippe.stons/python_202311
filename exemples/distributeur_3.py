def get_number(msg):
    while True:
        try:
            nbr = int(input(msg))
            return nbr
        except ValueError:
            print("Vous devez entrer un nombre")

def get_distrib_message(content: dict):
    msg = "Entrer "
    for key, val in content.items():
        if val["qty"] != 0:
            msg += f'({key}) {val["name"]} '
            # msg += f'({key}) {val["name"]} '

    msg += " (100) pour quitter ou (101) pour lister : "

    return msg

def distrib_input(content: dict):
    selection = 102
    msg = get_distrib_message(content)
    while not selection in content.keys() and selection != 101 and selection != 100:
        selection = get_number(msg)

    return selection

def manage_distrib_input(selection, content: dict):
    if selection < 100 and content[selection]["qty"] > 0:
        content[selection]["qty"] -= 1
        print(f"Voici votre {content[selection]['name']}")
    elif selection == 101:
        for key, elem in content.items():
            print(key, elem['name'], elem['qty'])
    elif selection != 100:
        print("Cette boisson n'est plus disponible!")

def distributeur(content: dict):
    selection = 102
    while selection != 100:
        selection = distrib_input(content)
        manage_distrib_input(selection, content)

content = {
    55: { "name": "coca", "qty": 5 },
    74: { "name": "fanta", "qty": 0 },
    32: { "name": "sprite", "qty": 12 },
    12: { "name": "eau", "qty": 2 },
    24: { "name": "yoplait", "qty": 6 },
    64: { "name": "pepsi", "qty": 6 },
}

distributeur(content)

