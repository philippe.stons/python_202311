"""
Simuler un distrubuteur de boisson, un utilisateur peut demander une boisson, si il en reste. Le distrubeur donne la
boisson et la retire de son inventaire; sinon le distributeur alerte l'utilisateur qu'il n'y a plus de stock restant.
À tout moment l'utilisateur peut décider de quitter le distributeur.
"""

content = {
    55: { "name": "coca", "qty": 5 },
    74: { "name": "fanta", "qty": 0 },
    32: { "name": "sprite", "qty": 12 },
    12: { "name": "eau", "qty": 2 },
    24: { "name": "yoplait", "qty": 6 },
    64: { "name": "pepsi", "qty": 6 },
}

in_nbr = -1

while in_nbr != 99:
    print("Entrer 99 pour quitter, 100 pour lister l'inventaire ou l'un des numéro d'une boisson :")

    for i, drink in content.items():
        # je vérifie qu'il y a encore du stock de cette boisson avant de l'imprimer.
        if drink["qty"] > 0:
            print(i, drink["name"])

    in_nbr = int(input("Choix : "))

    # je vérifie que le nombre est bien plus petit que 99 (qui sert à quitter le programme)
    # que celui-ci est bien un index valide dans le contenu (entre 0 et len(contenu) - 1)
    # et qu'il reste encore de cette boisson (qty > 0)
    if in_nbr < 99 and in_nbr >= 0 and in_nbr in content.keys() and content[in_nbr]["qty"] > 0:
        content[in_nbr]["qty"] -= 1
        print("Voici votre", content[in_nbr]["name"])
    # si l'utilisateur entre 100 on liste le nom et la quantité restante de la boisson
    elif in_nbr == 100:
        for drink in content.values():
            print(drink["name"], drink["qty"])
    elif in_nbr == 99:
        print("Bye")
    else:
        if in_nbr in content.keys():
            print(f"Erreur, il n'y a plus de {content[in_nbr]['name']} en stock")
        else:
            print("Erreur, la boisson n'existe pas")
