def bubble_sort(tab):
    sort = False
    count = 0
    while(not sort):
        sort = True
        for i in range(1, len(tab) - count):
            if(tab[i] < tab[i - 1]):
                tab[i], tab[i - 1] = tab[i - 1], tab[i]
                sort = False
        count += 1


tab = [5, 1, 3, 2, 9, 8]
bubble_sort(tab)
print(tab)
print(itteration)